#!/usr/bin/env node
const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const argv = yargs(hideBin(process.argv)).argv

let data = [
    {id: 1, valor: 10, desc: 'uno'},
    {id: 2, valor: 20, desc: 'dos'},
    {id: 3, valor: 30, desc: 'tres'},
    {id: 4, valor: 40, desc: 'cuatro'},
    {id: 5, valor: 50, desc: 'cinco'},
  ]

const result = data.filter(row => row.id === argv.valor)
console.log(result)